package com.mine.docker.restControllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Artem Pozdeev on 18.05.2016.
 */
@RestController
public class practiceController {
    @RequestMapping("/")
    public String getStart(){

        return "Start Rest";
    }
    @RequestMapping("/go")
    public String getGo()
    {
        return "Go-Go-Go";
    }
    @RequestMapping("/doc")
    public String home() {
        return "Hello Docker World!";
    }
}
